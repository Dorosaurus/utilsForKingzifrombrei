#include <DynRPG/DynRPG.h>
#include <list>
#include <math.h>

using namespace std;

list< pair<int,int> > animatedEvents; //first: event id, second: timeout frame


void onFrame(RPG::Scene scene)
{
    list< pair<int,int> >::iterator it = animatedEvents.begin();
    while (it != animatedEvents.end())
    {
        RPG::Event *ev = RPG::map->events[it->first];

        if (it->second > RPG::system->frameCounter)
        {
            it++;
        }
        else
        {
            ev->setAnimationType(RPG::ANI_NORMAL);
            it = animatedEvents.erase(it);
        }
    }
}

// Ver�ndert den Pixeloffset der Eventgrafik in Abh�ngigkeit der momentanen Position
// Pixeloffset bleibt immer in [-8,8], bei �berschreitung wird Gridposition ver�ndert
void addEventOffset(int id, int offset_x, int offset_y)
{
    int target_offset_x = RPG::map->events[id]->offsetX + offset_x;
    int target_offset_y = RPG::map->events[id]->offsetY + offset_y;

    if (target_offset_x > 8)
    {
        int grid_change_x = (target_offset_x + 8) / 16;
        RPG::map->events[id]->x += grid_change_x;
        RPG::map->events[id]->offsetX = (target_offset_x - (grid_change_x * 16));
    }
    else if (target_offset_x < -8)
    {
        int grid_change_x = (target_offset_x - 8) / 16;
        RPG::map->events[id]->x += grid_change_x;
        RPG::map->events[id]->offsetX = (target_offset_x - (grid_change_x * 16));
    }
    else
    {
        RPG::map->events[id]->offsetX = target_offset_x;
    }

    if (target_offset_y > 8)
    {
        int grid_change_y = (target_offset_y + 8) / 16;
        RPG::map->events[id]->y += grid_change_y;
        RPG::map->events[id]->offsetY = (target_offset_y - (grid_change_y * 16));
    }
    else if (target_offset_y < -8)
    {
        int grid_change_y = (target_offset_y - 8) / 16;
        RPG::map->events[id]->y += grid_change_y;
        RPG::map->events[id]->offsetY = (target_offset_y - (grid_change_y * 16));
    }
    else
    {
        RPG::map->events[id]->offsetY = target_offset_y;
    }
}

int getEuclidianDistance(int x_1, int y_1, int x_2, int y_2)
{
    int x_dif = x_1 - x_2;
    int y_dif = y_1 - y_2;
    return sqrt(x_dif * x_dif + y_dif * y_dif);
}

bool onComment(const char *text, const RPG::ParsedCommentData *parsedData,
               RPG::EventScriptLine *nextScriptLine,
               RPG::EventScriptData *scriptData, int eventId,
               int pageId, int lineId, int *nextLineId)
{
    // Setzt den Pixeloffset, ohne Bounds [-128,128] zu checken und ohne Gridposition zu ver�ndern
    if (strcmp(parsedData->command, "set_event_offset") == 0)
    {
        // param 0: event ID
        // param 1: x-offset
        // param 2: y-offset
        int eventId = parsedData->parameters[0].number;
        if (parsedData->parameters[1].type == RPG::PARAM_NUMBER)
            RPG::map->events[eventId]->offsetX = parsedData->parameters[1].number;
        if (parsedData->parametersCount > 2){
            if (parsedData->parameters[2].type == RPG::PARAM_NUMBER)
                RPG::map->events[eventId]->offsetY = parsedData->parameters[2].number;
        }
        return false;
    }

    // Addiert Pixeloffset zur momentanen Pixelposition, ver�ndert Gridposition ggf.
    if (strcmp(parsedData->command, "add_event_offset") == 0)
    {
        // param 0: event ID
        // param 1: x-offset
        // param 2: y-offset
        int event_id = parsedData->parameters[0].number;
        addEventOffset(event_id, parsedData->parameters[1].number, parsedData->parameters[2].number);
        RPG::map->events[event_id]->setAnimationType(RPG::ANI_STEPPING);

        for (list< pair<int,int> >::iterator it = animatedEvents.begin(); it != animatedEvents.end(); it++)
        {
            if (it->first == event_id)
            {
                animatedEvents.erase(it);
                break;
            }
        }
        animatedEvents.push_back(pair<int,int>(event_id, RPG::system->frameCounter + 1));

        return false;
    }

    // Gibt momentanen Pixeloffset in Variablen zur�ck
    if (strcmp(parsedData->command, "get_event_offset") == 0)
    {
        // param 0: event ID
        // param 1: var id f�r x-offset
        // param 2: var id f�r y-offset
        int eventId = (int)parsedData->parameters[0].number;
        RPG::variables[(int)parsedData->parameters[1].number] = RPG::map->events[eventId]->offsetX;
        RPG::variables[(int)parsedData->parameters[2].number] = RPG::map->events[eventId]->offsetY;
        return false;
    }

    // Setzt die globale Pixelposition auf der Map, ver�ndert Gridposition ggf.
    if (strcmp(parsedData->command, "set_event_global_pos") == 0){
        // param 0: event ID
        // param 1: map x (pixel)
        // param 2: map y (pixel)
        int eventId = parsedData->parameters[0].number;

        int correction_x = 0;
        int correction_y = 0;

        if (parsedData->parameters[1].type == RPG::PARAM_NUMBER)
        {
            int map_x = RPG::map->getCameraX() + RPG::map->events[eventId]->getScreenX();
            correction_x = (parsedData->parameters[1].number - map_x);
        }

        if (parsedData->parameters[2].type == RPG::PARAM_NUMBER)
        {
            int map_y = RPG::map->getCameraY() + RPG::map->events[eventId]->getScreenY();
            correction_y = (parsedData->parameters[2].number - map_y);
        }

        addEventOffset(eventId, correction_x, correction_y);
        return false;
    }

    if (strcmp(parsedData->command, "get_distance") == 0){
        // param 0: event ID 1 (0 fuer hero)
        // param 1: event ID 2 (0 fuer hero)
        // param 2: var id fuer distanz

        RPG::Character* ev1, *ev2;

        if (parsedData->parameters[0].number == 0)
            ev1 = RPG::hero;
        else
            ev1 = RPG::map->events[parsedData->parameters[0].number];

        if (parsedData->parameters[1].number == 0)
            ev2 = RPG::hero;
        else
            ev2 = RPG::map->events[parsedData->parameters[1].number];

        RPG::variables[parsedData->parameters[2].number] = getEuclidianDistance(ev1->getScreenX(), ev1->getScreenY(),
                                                                                ev2->getScreenX(), ev2->getScreenY());

        return false;
    }

    return true;
}
